# Welcome to qsim

Welcome to the MPSD tutorial for quantum simulations (qsim).

© 2021

* Heiko Appel (heiko.appel@mpsd.mpg.de)
* Henning Glawe (henning.glawe@mpsd.mpg.de)
* Hans Fangohr (hans.fangohr@mpsd.mpg.de)
* Nicolas Tancogne-Dejean (nicolas.tancogne-dejean@mpsd.mpg.de)
* Martin Lueders (martin.lueders@mpsd.mpg.de)

[Max Planck Institute for the Structure and Dynamics of Matter, Hamburg](https://mpsd.mpg.de/)


# Overview

The qsim project and tutorials are divided into three different parts

1. Tutorials for basic skills in HPC computing
2. Basic introduction to a few selected numerical algorithms
3. Building a quantum simulation package (future project)

In all cases a strong emphasis is placed on 'learning by doing'. We provide
command promt examples and code snippets which should provide a good starting point
to enhance your skills for numerical simulations in modern high-performance
computing environments.

The git repository for the qsim project can be found under

 [https://gitlab.com/heiko.appel/qsim](https://gitlab.com/heiko.appel/qsim)

and the documentation for the course is online available via

 [https://heiko.appel.gitlab.io/qsim](https://heiko.appel.gitlab.io/qsim)

## Tutorials for basic skills in HPC computing

In the first part of the course, we cover the following topics

- Basic introduction to revision control with git
- Using Gitlab
- Creating own Python packages
- Screen multiplexing with tmux
- Environment modules
- Using the slurm queueing system
- Dotfiles

## Basic introduction to a few selected numerical algorithms

The second part of the course is providing a lecture on selected numerical
methods.

## Building a quantum simulation package

This part is covered in future courses, but provides already the name for
the project.

## Status

[![pipeline status](https://gitlab.com/heiko.appel/qsim/badges/master/pipeline.svg)](https://gitlab.com/heiko.appel/qsim/pipelines)
[![coverage report](https://gitlab.com/heiko.appel/qsim/badges/master/coverage.svg)](https://gitlab.com/heiko.appel/qsim/commits/master)
