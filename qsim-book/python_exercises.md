# Python Exercises

After having attended the Python course, you should now all be in a position to use Python for
completing the programming tasks in the following section of exercises.

Depending on the complexity of the task, you might be asked to create the whole program, or to write or complete the main functions of the code, doing the work. In this case, some scaffolding (also called boiler plate code) will already be provided as part of the git repository you already cloned.
