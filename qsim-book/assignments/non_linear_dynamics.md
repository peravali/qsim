# Non-linear dynamics

## Non-linear pendulum

Consider the equations of motion for a driven, non-linear pendulum in
dimensionless form:

$$
  \ddot{\varphi} + r\dot{\varphi} + \sin\varphi = \alpha \cos(\omega t)
$$ (PendelODE)

The parameters $r,\,\alpha$ and $\omega$ describe the damping of the system,
the amplitude and the frequency of the driving force.

1) Implement the following 4th order Runge-Kutta method:

    $$
    {\bf k_1} & = & h\,{\bf f}(x_n,{\bf y_n}) \\
    {\bf k_2} & = & h\,{\bf f}(x_n+\frac{h}{2},{\bf y_n}+\frac{1}{2}\,{\bf k_1}) \\
    {\bf k_3} & = & h\,{\bf f}(x_n+\frac{h}{2},{\bf y_n}+\frac{1}{2}\,{\bf k_2}) \\
    {\bf k_4} & = & h\,{\bf f}(x_n+h,{\bf y_n}+{\bf k_3}) \\
    {\bf y_{n+1}} & = & {\bf y_n} + \frac{1}{6} \, {\bf k_1} + \frac{1}{3} \, {\bf k_2}
    + \frac{1}{3} \, {\bf k_3} + \frac{1}{6} \, {\bf k_4}  + O(h^5)
    $$

    Here $x_n=x_0+n\,h$ are abscissae with equidistant spacing $h$ and ${\bf
    y_n}$ is the vector with the approximate values for the solution of the
    system of equations.

    Linearize the equations of motion of the pendulum {eq}`PendelODE` for the
    free and undamped case ($r=\alpha=0$).  Determine the solution of the
    resulting equation for the initial conditions $\varphi(0)=\pi/3$ and
    $\dot{\varphi}(0)=0$ and test your implementation of the Runge-Kutta method
    for different step sizes $h$ by comparison with the exact solution.

2) Use your routine to determine the solution of equation {eq}`PendelODE` (i.e.
   without linearization) for the case $r=\alpha=0$.  Plot the phase-space
   trajectories for different initial conditions $\varphi(0),\,\dot{\varphi}(0)$.
   Comment your observations! What changes qualitatively in the case of a damped
   oscillation ($r\neq 0$)?  What do you observe when you switch on the external
   driving force ($r\neq 0, \alpha\neq0$)?  Investigate different combinations of
   amplitude $\alpha$, frequency $\omega$ and damping parameters $r$ and plot your
   results


## Lorenz-Attractor

![ring](convection.png)

The description of the Rayleigh-B&eacute;nard-Convection of a fluid in a
circular tube under the influence of gravity leads after some approximations to
the so-called Lorenz-model, which is defined by the following set of
differential equations:

$$
  \dot{x} & = & -\sigma(x-y) \\
  \dot{y} & = & rx-y-xz \\
  \dot{z} & = & -bz + xy
$$

The derivation of these goes beyond the scope of this exercise. Here we will
only investigate the dynamics, which results from these equations.

Extend your code from the non-linear pendulum such, that it can be applied to
the above equations.  Consider the Lorenz model in the canonical case
($\sigma=10,\,b=8/3$) for parameters $0 < r < 1$.  What can you say about the
form of the solutions for various initial conditions of $x(0),\,y(0),\,z(0)$?
How does the behavior change for parameters $1<r<r_c$ with

$$
r_c = \frac{\sigma(\sigma+b+3)}{(\sigma-b-1)},
$$

and what do you observe for $r>r_c$? Depict the $(x(t),y(t),z(t))$, with
$t\in[0,T]$ for the analysis of the solutions of the Lorenz system as
trajectories in 3-dimensional space.


