# Numerical Accuracy (1)
## Summation of series

Consider the following alternating sum:

$$
  \arctan(x) = \sum_{\nu=0}^{\infty} (-1)^\nu 
  \frac{x^{2 \nu + 1}}{2 \nu + 1} \qquad \mbox{for} \qquad |x| \le 1
$$

As this problem is not solvable exactly on a computer, we need to find a
substitute problem. One possibility is the truncated sum with a finite number
of terms:

$$S
 _n(x) = \sum_{\nu=0}^{n-1} (-1)^\nu 
         \frac{x^{2 \nu + 1}}{2 \nu + 1} 
         \qquad \mbox{for} \qquad |x| \le 1
$$

Write a Python program to determine by direct summation of $S_n(x)$ the value
of $n$ for which the *relative* error gets smaller than $10^{-6}$

Write a function with the following signature:
```Python
def arctan_approx_series(x, precision):
    """
    Approximate arctan(x) to given precision.

    Input: 
        x0:        argument at which arctan() shall be evaluated
        precision: desired precision

    Returns: tuple of three values:
        (approximation of arctan(x) to given precision, relative error, number of terms)

    Evaluation by summation of the series
    $S_n(x) =  \sum_{\nu=0}^{n-1} (-1)^{\nu} \frac{x^{2\nu+1}}{2\nu+1}$
    """

    return (0, 0, 0)
```

Remember, that $\arctan(1) = \pi/4$.


An obvious substitute problem is not necessarily the most suitable one for a
given problem. Consider as an alternative to the previous, the following
algorithm for summing the series (a special case of the continued
arithmetic-geometric mean calculation):

- start:     $a_0 = (1 + x^2)^{-1/2}$, $b_0 = 1$,
- iteration: $a_{i+1} = \frac{1}{2}(a_i + b_i)$, $b_{i+1} = \sqrt{a_{i+1} b_i}$,  $i = 0, \dots, n-1$,
- stop:      $|a_n - b_n| < \delta$
- result:    $\arctan(x) \approx \frac{x}{(1+x^2)^{1/2} a_n}$.

Write a function `arctan_approx_means()` with the same signature as above and
implement this algorithm. Again, determine the number of terms necessary for a
precision of $10^{-6}$.

For which $n$ do you now get the same relative error as in part (a)? Estimate
by counting the number of floating point operations (FLOPS), required for an
relative accuracy of $10^{-6}$. For this estimate, assume that the calculation
of the square root requires 18 FLOPS. Perform timing measurements on your
Python code and check whether the estimate is met in reality.


## Floating point arithmetic

The machine precision `eps` can be determined by the following pseudo-code:

1. `eps` = 1,
   `alpha` = 1
2. Divide `eps` by 2
3. Compare `alpha` with `alpha` + `eps`.
   If both are identical, 2*`eps` is the machine accuracy.
   If not, continue with step 2.


Write a program to determine the machine accuracy. Which values do you obtain with
1. `alpha` = $1$,
2. `alpha` = $10^{-4}$ and
3. `alpha` = $10^{11}$ ?

What exactly does the machine accuracy describe? Why would be it wrong if we divided `eps` by 3 in step 2.?
