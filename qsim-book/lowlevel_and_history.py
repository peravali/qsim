#!/usr/bin/env python3
# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:hydrogen
#     notebook_metadata_filter: rise
#     text_representation:
#       extension: .py
#       format_name: hydrogen
#       format_version: '1.3'
#       jupytext_version: 1.10.3
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
#   rise:
#     autolaunch: true
#     enable_chalkboard: true
#     theme: white
#     transition: none
#     slideNumber: true
# ---

# %% [markdown] slideshow={"slide_type": "slide"} tags=[]
# # History of Computers
# - explains terminology and reception in society
# - (and how computers work)
#
# <img style="float: right;" src="images/jaquard-loom.jpg" width=40%>
#
# ## Loom
# - 4400 BC: device to weave threads into cloth
# - Joseph Jacquard (1805): industrial revolution, programmed patterns:
#   * processor (the Jacquard loom) executes
#   * instructions following a program given by
#   * punch cards (cardboard sheets with a pattern of holes)
#   * output: cloth with fancy color patterns
#
# ## Two different branches in future development
# - computers
# - databases
#
# %% [markdown] slideshow={"slide_type": "slide"} tags=[]
# ## 19th Century
# ### Computers: Analytical engine
# - Charles Babbage (1837): postulated mechanical computer (program on punch cards)
# - Ada Lovelace (1843): defined "software", "hardware" and "program"
#   (considered the first programmer)
# - was not built back then (insufficient money)
# - first "universal computer" (was later proved to be *Turing-complete*)
# - spawned the "what-if" SciFi sub-genre "Steampunk"
#   (_Gibson/Sterling_: *The Difference Engine*)
#
# ### Databases: Hollerith (company later renamed to "IBM")
# - Jacquard punch cards
# - sorting/searching datasets w.r.t.  attributes
# - (questionable role later in Nazi Germany)
#   * result today: increased data privacy concerns in DE and EU (e.g.: GDPR)
# - origin of standard ```IBM punch cards``` (*Watson* 80 characters per card)
#
# %% [markdown] slideshow={"slide_type": "slide"} tags=[]
# ### Punch card deck
#
# <img src="images/Punched_card_program_deck.agr.jpg" alt="Deck of punch cards" style="width: 60%;"/>
#
# %% [markdown] slideshow={"slide_type": "slide"} tags=[]
# ## Early 20th century computers
# - Konrad Zuse
#   * Z1 (1937)
#     + structural analysis calculations (architecture)
#     + first freely programmable computer with boolean logic and binary
#       floating-point numbers
#     + mechanical (driven by a vacuum cleaner motor 😉) and unreliable
#     + programmed via punch-tapes
#   * Z3 (1941) electro-mechanical (relais)
#   * Zuse computers were also involved in WW2 weapon design
#     (Henschel Hs 293 guided bomb), alternative: **back to the front**
# - Manhatten project (1940s)
#   * mostly used analog computers/calculators
# - ENIAC (1950s)
#   * no floating-point
#   * "fast", because electronic (vacuum tubes)
#   * had to be programmed by "patching" cables
#   * not yet ready during WW2, but used to develop fusion bomb
# - in the UK: COLOSSUS, Manchester Baby (mid to end 1940s)
#
# %% [markdown] slideshow={"slide_type": "slide"} tags=[]
# ### Zuse Z1 (reconstructed, Berlin Museum of technology)
# <img src="images/1080px-German_Museum_of_Technology,_Berlin_2017_024.jpg" alt="Zuse Z1 (reconstructed, Berlin Museum of technology)" style="width: 60%;"/>
#
# %% [markdown] slideshow={"slide_type": "slide"} tags=[]
# ## Formalization of "universal computers"
# - Alan Turing: theoretical basis: definition of *Turing-Complete* (1948)
#   * universal programmability
#   * i.e. can compute any mathematical function
# - John von Neumann Architecture (also late 1940s):
#   * control unit (CU) and arithmetic logic unit (ALU), both on the same chip nowadays (CPU, central processing unit)
#   * common memory for data and instructions, what we call Random Access Memory (RAM) nowadays
#   * external mass storage
#   * input/output mechanisms
# - later computers in the West used Turing/von-Neumann design
#
# %% [markdown] slideshow={"slide_type": "slide"} tags=[]
# ## Definition of data units
# with digital computers, everyone agrees what a ```bit``` is:
#   either 1 or 0 (BInary digiT), SI unit ``1b``
# ### How many bits are a "byte"?
#   * historical computers used various numbers of ``b`` for a byte
#   * present convention is ``8b``, SI unit ``1B`` (also called ``octet``)
#     + ``4b`` are sometimes called a ``nibble``, value range 0..15
#     + nibble is a hexadecimal (base 16) digit,
#       representation: {0..9,a,b,c,d,e,f} (sometimes also uppercase), prefix '0x'
#     + so each ``B`` can be represented by a 2-digit hex number
#       - Debugging/Lowlevel: in case of errors, a **hexdump** is displayed (``[255, 216, 255, 224]`` ->  ``FF D8 FF E0``)
#       - use a **hex-editor** to really see what is wrong with files and fix it
#   * confusion around unit prefixes: ``k`` is 1000 in SI, but base-2 would
#     prefer 1024; story continues for ``M``, ``G``, ``T``...
#     - clarification ``ki``, ``Mi`` ... ("kibi", "mebi", ...)
#
# %% [markdown] slideshow={"slide_type": "slide"} tags=[]
# ### Integer numbers
# - also used for addressing memory (``pointers``)
# - and for storing date/time (unix timestamp: number of seconds since ``1970-01-01 00:00:00 UTC``)
# - unsigned: ``1B`` range $0 \ldots (2^8-1=255)$
# - signed (sign requires ``1b``):
#   * negative numbers: *2-complement* (implementation for ``+``, ``-``,
#     ``*`` stays the same as for unsigned), so ``-1`` is ``0xFF``
#   * value range $(-2^7=-128) \ldots (2^7-1=127)$
# - increase range: increase number of bytes (exponential growth of range):
#
# |  ``b`` | unsigned max |        signed min |            signed max |
# |-------:|-------------:|------------------:|----------------------:|
# |       8|           255|               -128|                    127|
# |      16|        65.535|            -32.768|                 32.767|
# |      32| 4.294.967.295|     −2.147.483.648|          2.147.483.647|
# |      64| 18.446.744.073.709.551.615| −9.223.372.036.854.775.808 | 9.223.372.036.854.775.807 |
#
# %% [markdown] slideshow={"slide_type": "slide"} tags=[]
# #### Big-Endian / Little-Endian
#
# - how to order "bytes" for larger integers in memory and IO (depends on CPU architecture)
# - ``big-endian``: trivial idea, just store as-is (int32: 4, 3, 2, 1)
# - ``little-endian``: common today (int32: 1,2,3,4), but why?
#   + access the same ``pointer`` as (int8, int16, int32, ...),
#     values are the same (if close enough to 0)
# - very important when transferring binary data between computers
# - "Network Byte order" is big-endian, which is used in many standard
#   TCP/IP protocols. So when you are implementing, you need to convert to
#   host byte order...
# - you may also need to convert binary files generated on HPCs for analysis
#   on another HPC or your desktop/laptop (better use HDF5 😉)
#
# %% [markdown] slideshow={"slide_type": "slide"} tags=[]
# ### Approximate real numbers $n\in\mathbb{R}$
#   * real number can only be treated approximately (finite number of ``b``)
#   * ``fixed-point``: postulate a ``"decimal point"`` between integer's bits
#     - fast, no extra hardware required (just use integer ALUs)
#     - but restricted value-range
#     - frequently used in GPUs (when playing a game, frames-per-second
#       (FPS) matter, but not the mathematical accurracy of each frame)
#   * ``BCDs`` (*binary-coded decimals*), important in finance/bookkeeping
#     (laws for financial rounding could be violated by binary arithmetics)
#   * introduce ``floating-point numbers`` (but you need additional hardware: FPU)
#     - split number into ``sign``, ``mantissa`` and base-2 ``exponent``:
#
# |   description | width | sign | Mantissa | Exponent |               Range |
# |--------------:|------:|-----:|---------:|---------:|---------------------|
# |        Zuse-Z1|``24b``|``1b``|   ``16b``|    ``7b``|  $0.922\cdot10^{19}$|
# |IEEE 754 single|``32b``|``1b``|   ``23b``|    ``8b``| $\pm3.4\cdot10^{38}$|
# |IEEE 754 double|``64b``|``1b``|   ``52b``|   ``11b``|$\pm1.7\cdot10^{308}$|
#
# %% [markdown] slideshow={"slide_type": "slide"} tags=[]
# ## The really annoying part: ``text`` or ``characters``
# - computer does not know human-readable characters, only integers
# - ``character map``: maps integers to human-readable characters
# - ``ASCII`` (1968): ``7b`` (many other existed)
#   * ``00``-``1f``: ``32`` control characters
#   * ``20``-``7f``: Latin alphabet + interpunctation
# - what about non-latin characters?
#   * old approach: abuse bit ``8``
#   * as metadata, you have to know the ``codepage/encoding`` (as there are more
#     than 256 different characters in the whole world):
#     + ``western-european``, ``cyrillic``, ``...``
#     + if you display a string assuming the wrong encoding, you get garbage on the screen
#
# %% [markdown] slideshow={"slide_type": "slide"} tags=[]
# ### Unicode text
# - define a "global" character map once and for all (``32bit``, but sparse)
# - but using ``32b`` for each character is a waste of space
# - again different encodings:
#   * old windows NT...2000 used UCS-2 (``16b`` subset of unicode)
#   * newer windows uses UTF-16 (``16b and assemble-by-escapes``)
#   * linux/macos uses UTF-8 (``8b and assemble-by-escapes``)
#     + advantage: byte-representation is mostly identical to ASCII
# - disadvantage: a ``character`` is not a ``byte`` anymore:
#   * more complicated handling in low-level languages
# - on the other hand, ``32b`` are even enough to encode emojis such as 😉
# - with ``16b`` or ``32b`` characters, you need to also know the Endian-Ness
#   of data in a file
#   * write a byte-order marker (``BOM``) at the beginning of the file
#   * unfortunately, some windows programs also write a ``BOM`` at the
#     beginning of ``UTF-8`` text files (which is not necessary!)
#   * lengthy debugging session in the python course:
#     + python code failed
#     + modern editors ignore the ``BOM`` (i.e. don't display it)
#     + had to use ``hex-editor`` to fix the file
#
# %% [markdown] slideshow={"slide_type": "slide"} tags=[]
# ### Text encoding in python3
# - ``python3`` distinguishes between ``str`` and ``bytes``:
# - remember [Lab4 of the python course](https://www.desy.de/~fangohr/teaching/py4cs2021/labs/lab4/index.html)?
# - ``noaa_data_bytes = urllib.request.urlopen(url).read()`` returns a ``bytes`` object
# - in order to convert this to a string, you had to use ``noaa_data_str=noaa_data_bytes.decode("utf-8")``
# - reverse operation would be ``noaa_data_bytes = noaa_data_str.encode("utf-8")``
#   * and you need this when uploading data in a ``post`` request ``urllib.request.Request(url, data=noaa_data_bytes)``
#   * or when writing to a binary file via ``file_obj.write(noaa_data_bytes)``
#   * for text files, encode/decode is implicitly done by the high-level python IO
#
# %% [markdown] slideshow={"slide_type": "slide"} tags=[]
# # Back to history: 1950's/1960's computers (remember "old" James Bond movies)
# - computers filled large rooms (with far less performance than you have on a smartphone or even smartwatch now)
# - and were so expensive that a university or company could only afford few
# - early workflow for a user:
#   1. produce a ``deck`` of punch cards (80 columns per card) with your program,
#      plus additional data cards
#   2. stand in queue in front of the operators' front desk to 'submit' it, and request resources
#   3. this ``batch job`` was then scheduled by the operators, based on the requested resources and priorities
#   4. receive the result as a standard-printout (STDOUT) in your letterbox
#   * using a line-printer explains also the importance of ``control characters`` such as ``line feed`` or ``carriage return``
# - introduction of a set of vendor-provided functions to handle things like IO
#   (operating system OS)
# - improvement: time-sharing OS time-slices compute resources, and distribute
#   fairly among jobs
#
# %% [markdown] slideshow={"slide_type": "slide"} tags=[]
# ## Terminals (breakthrough for interactive usage of a computer)
# * printer and keyboard in one device (1963, Teletype Model 33), that's why
#   your unix terminals are accessed via ``/dev/tty*``
# * 2 serial line connections between the "small" terminal in your office and
#   the mainframe computer occupying the whole basement of the company:
#   keyboard will become ``STDIN`` in UNIX, printer output ``STDOUT``
# * starting mid-60s, CRT terminals appeared (replacing the printer by a
#   monochrome Cathod-Ray-Tube screen)
#   + and of course, each manufacturer implemented different control sequences
#     for anything beyond CR/LF (such as explicit cursor positioning)
#   + standard: ANSI (1978), early hardware on the market: ``DEC VT-100``
#   + color terminal: ``DEC VT-241`` (1983)
#   + interestingly, development of terminals continued up to mid-90s
#     (personal/home computers with color and even graphical screens
#     were already common and GUI-capable X11 terminals existed)
# * command interpreter for user interaction (runs on the computer in the
#   basement): ``shell`` (ethymology is a bit unclear)
#
# %% [markdown] slideshow={"slide_type": "slide"} tags=[]
# ### So when you run ``xterm``, it emulates hardware like
# <img style="float: left;" src="images/960px-ASR-33_at_CHM.agr.jpg" width=40%>
# <img style="float: right;" src="images/811px-DEC_VT100_terminal.jpg" width=40%>
#
#
# %% [markdown] slideshow={"slide_type": "slide"} tags=[]
# # Unix (1973) operating system
# - clean up the "grown" developments of the past 40 years, introduce a
#   consistent design
# - multi-user, multi-tasking (time-slicing resources)
# - ``kernel`` with ``drivers`` abstracts hardware
# - *everything is represented by a file*
#   * see the ``/dev/`` tree on your linux system
#   * if devices provide something beyond [``read``, ``write``...] -> ``ioctl``
# - ``process``:
#   * running instance of a program (with 1 instruction pointer)
#   * automatically gets 3 filehandles: ``STDIN``, ``STDOUT`` and ``STDERR``
#   * walled garden of "private" memory
#   * can ``fork`` other ``proccesse``s
# - inter-process communication:
#   * ``pipe``: connect file handles of child and parent (unidirectional)
#   * ``fifo``: pseudo-file, similar to ``pipe``
#   * ``socket``: bi-directional connection between two ``processes``
#     (even running on another computer: ``TCP/IP`` sockets)
#   * and you still just use file-like read/write to communicate
#
# %% [markdown] slideshow={"slide_type": "slide"} tags=[]
# ## Unix "Plumbing": build complex things from simple programs
# - simple example: run an ``xterm``:
#   * ``xterm`` process is ``forked()`` by window manager, STD file handles
#     are duplicated from the parent
#     + remember: ``xterm`` just emulates ancient hardware like the ``vt-100``
#       serial-connected text terminal
#     + ``xterm`` in turn ``fork()s`` a ``shell``, with STD handles connected
#       to the serial ``xterm`` keyboard input and window output sockets
# - and the ``shell`` (in our case: ``bash``) provides a simple but powerful
#   syntax for plumbing child processes:
#   * ``blah |blubb``: establish a pipe between ``blah``'s STDOUT and
#     ``blubb``'s STDIN (reason why ``|`` is prounced as "pipe")
#   * ``blah >output_file.txt``: redirect **STDOUT**: pass a handle to
#     ``output_file.txt`` as **STDOUT** to ``blah``
#   * ``blah <input_file.txt``: redirect **STDIN**: pass a handle to
#     ``input_file.txt`` as **STDIN** to ``blah``
#   * and error/debug messages (STDERR) of any program in your pipeline
#     will still show up in ``xterm``
#   * this is why we can do things like
#     ```console
#     glawe@safflower:~> ps auxwww | grep xterm | grep -v grep | awk '{print $2}'
#     1731432
#     ```
#
# %% [markdown] slideshow={"slide_type": "slide"} tags=[]
# # Back to the hardware: what is a program and how does it run?
# - sequence of instructions to ``CU``, which run on ``CU``, ``ALU`` or ``FPU``
#   (remember the card-decks or punch-tapes), but we now assume a ``CPU``
# - CPU can advance or go back in the sequence conditionally (Turing)
# - an instruction is implemented in hardware
#   * works on CPU *registers* (on amd64: 16 int64)
#   * is encoded as a short sequence of bytes (OPCODE) and usually has <=2 operands
#   * takes 1 or more clock cycles to execute
#     + e.g. ``fdiv`` takes 10 times longer then ``fmul``
#     + memory access takes many cycles (depending on cache etc.)
#   * a bit more convenient representation: assembler (OPCODE and operands
#     in a human-readable form, but 1-to-1 map to opcodes)
# ```x86asm
# mov eax, DWORD PTR [var1]
# mul eax, DWORD PTR [var2]
# mov DWORD PTR [var3], eax
# ```
#     and x86 FPU code gets even more complicated, as the FPU registers
#     are implemented as a ``stack``
#
# %% [markdown] slideshow={"slide_type": "slide"} tags=[]
# # Compiled and interpreted programming languages
# - *life is too short to do everything in assembler*
# - and even if you have an assembler, each CPU architecture uses its own
#   instruction set and therefore assembly-language representation
# - *portability* and convenience requires higher-level language
#   * ``Fortran`` (For(mula) Tran(slator), 1956)
#   * the language ``C`` evolved alongside Unix, making Unix portable
# the CPU only knows about ``opcodes+operands``, so we need a translator
# from human-understandable ``source code``:
# - ``assembler`` goes back to Ada Lovelace (19th century): human-readable
#   representation of CPU instructions
# - ``compiler``: translates high-level-language programs into CPU
#   instructions; can perform various degrees of optimization
# - ``interpreter``: interprets user input at runtime, and issues
#   pre-compiled CPU instruction sequences depending on the interpretation
#   * obvious example: ``shell``, as you use this interactively
#   * also ``python``, ``matlab``, ``perl``
#   * cannot optimize as much as a compiler (possible interactivity)
#   * but development could profit from ``no need to run compiler/linker``
# - cutting the interpreter out and instead compiling into machine code
#   will always be faster
#
# %% [markdown] slideshow={"slide_type": "slide"} tags=[]
# # Errors you may see when running your own programs
# * remember: ``process`` and it's walled-garden memory
#   - SIGSEGV (segfault): you accessed memory outside your garden
#   - SIGBUS (bus error): you accessed multi-byte integer with incompatible
#     alignmnent
#
# %% [markdown] slideshow={"slide_type": "slide"} tags=[]
# ## Stack-Overflow (a bit harder to detect and explain)
# - a limited part of your processes' memory is allocated ``stack``
# - when you call a function (happens automatically)
#   + return address is stored on stack (where that function was called from)
#   + reserve memory on top of the stack for local variables
# - you need a stack, think about recursive functions...
# - Error: you go too deep in the recursion, exceeding the reserved stack mem
# - Error: iterating over a local array, you excceed the stack-allocated size
#   + and you may even overwrite the return adress
#   + a common attack point for hackers,  worst-case:
#     * you read unverified data from the net into a local array (on the stack)
#     * data contains ``evil`` code.
#     * and overwrites the return address on the stack with the entry point
#       of the ``evil`` code.
#     * when your function ends, the ``evil`` code will be executed
