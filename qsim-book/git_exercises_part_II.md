# Git Exercises, part II

## Using repositories on gitlab.com

:::{admonition} Exercise: getting familiar with gitlab.com
:class: seealso
1. Create an account on gitlab.com and upload your SSH key if you have one (instructions how to generate
   a key can be found under [Creating a SSH Key](misc.html#creating-a-ssh-key))
2. Create a project test-repo or similar on gitlab.com. Add the usernames of your fellow students
   to this gitlab project and give them developer access. Clone your own project and the projects
   of the others, create branches, add some commits and push the results to gitlab.com. Merge
   the results of your fellows into your master branch.
:::


## Git rebase

:::{admonition} Exercise: git rebasing
:class: seealso
Run commands [git-30]..[git-53] below and inspect the resulting effects on your test repositories.
:::

In this excercise, we investigate the difference of rebasing a feature branch on a master branch
and vice versa. To get started create a new test repository
```shell
[git-30] > git init rebase_example
[git-31] > cd rebase_example
```

Create a few commits in this repository (either manually), or using a loop
```shell
[git-32] > for x in $(seq 1 5); do echo master_$x > master_$x.txt; git add master_$x.txt; git commit master_$x.txt -m "commit #master_$x"; done
```

Next, we create a feature branch
```shell
[git-33] > git checkout -b feature_branch
```

and add more commits to the feature branch
```shell
[git-34] > for x in $(seq 1 5); do echo feature_$x > feature_$x.txt; git add feature_$x.txt; git commit feature_$x.txt -m "feature commit #$x"; done
```

After that we switch to the master branch and add some more commits there
```shell
[git-35] > git checkout master
[git-36] > for x in $(seq 6 10); do echo master_$x > master_$x.txt; git add master_$x.txt; git commit master_$x.txt -m "commit #master_$x"; done
```

Now we leave the repository and create two copies
```shell
[git-37] > cd ..
[git-38] > cp -a rebase_example rebase_feature_on_master
[git-39] > cp -a rebase_example rebase_master_on_feature
```

### Rebase feature_branch on master branch

At this point we have identical repositories and in the next step we test different orders for the
rebase

```shell
[git-40] > cd rebase_feature_on_master
[git-41] > git checkout feature_branch
[git-42] > git rebase master
[git-43] > cd ..
```
### Rebase master on feature_branch

and the other way around

```shell
[git-44] > cd rebase_master_on_feature
[git-45] > git checkout master
[git-46] > git rebase feature_branch
[git-47] > cd ..
```

Compare the histories in the two folders. How do they differ?


## Interactive rebase

Create a test repository to experiment with interactive git rebasing. As before, we use
a loop in the bash shell to generate a sequence of commits
```shell
[git-48] > git init interactive_rebase_example
[git-49] > cd interactive_rebase_example
[git-50] > for x in $(seq 1 5); do echo $x > $x.txt; git add $x.txt; git commit $x.txt -m "commit #$x"; done
```

In the next step, call interactive git rebase with the argument HEAD~4 (which is synonymous with
HEAD~~~~, which is synonymous with the (parent of (parent of (parent of (parent of HEAD)))))

```shell
[git-51] > git rebase -i HEAD~4
```

Your default editor opens and you are confronted with output similar to
```shell
pick 15619c3 commit #2
pick ee1b11b commit #3
pick 0d73ade commit #4
pick 6e00501 commit #5

# Rebase 8d7c239..6e00501 onto 8d7c239 (4 commands)
#
# Commands:
# p, pick <commit> = use commit
# r, reword <commit> = use commit, but edit the commit message
# e, edit <commit> = use commit, but stop for amending
# s, squash <commit> = use commit, but meld into previous commit
# f, fixup <commit> = like "squash", but discard this commit's log message
# x, exec <command> = run command (the rest of the line) using shell
# b, break = stop here (continue rebase later with 'git rebase --continue')
# d, drop <commit> = remove commit
# l, label <label> = label current HEAD with a name
# t, reset <label> = reset HEAD to a label
# m, merge [-C <commit> | -c <commit>] <label> [# <oneline>]
# .       create a merge commit using the original merge commit's
# .       message (or the oneline, if no original merge commit was
# .       specified). Use -c <commit> to reword the commit message.
#
# These lines can be re-ordered; they are executed from top to bottom.
#
# If you remove a line here THAT COMMIT WILL BE LOST.
#
# However, if you remove everything, the rebase will be aborted.
```

### Swap commits
As first example swap the commit lines #3, and #4, write the commit
message, and quit your editor. After that inspect the history of your
repository.


### Drop a commit

Invoke again
```shell
[git-52] > git rebase -i HEAD~4
```

and now replace the word *pick* for commit #3 with *drop*. Write the
file and leave again the editor. What happened to your commit history?

### Squash commits

Execute now (note ~4 is replaced by ~3 since we have one commit less
because of the previous drop)

```shell
[git-53] > git rebase -i HEAD~3
```

and in this case replace the word *pick* for commit #5 with *squash*.
This opens again an editor which allows you to edit the commit message
for the combined commit of #4 and #5. Adapt the message, write, and quit,
and inspect again the history of your repository.


# Best practices for git

 - If possible, always create atomic commits. This means that you split your file changes
   in as small units as possible and commit them all separately. This has the advantage
   that others can better follow your changes, you have a more granular time back machine,
   and when bugs are introduced it is easier to find them manually or with tools like git bisect.
   The opposite would be one commit per day with commit message "Work of today". This is
   not recommended.
 - Do not store large binary files in the repository. Git was designed to work with text
   files. While it is possible and often needed to also story binary files (such as images, pdf
   files, etc) in a git repository, make sure that these binary files do not get too large.


# What next?

Since git has become so popular and established, there is a multitude of tutorials, books,
and other types of references available on the web. Below we list a few for further reading.

## Pro Git book
A good reference for git is the Pro Git book. It covers most of the relevant
aspects and provides also the concepts behind git

[https://git-scm.com/book/en/v2](https://git-scm.com/book/en/v2)

## A visual git reference

The visual git reference of Mark Lodato is a very useful resource to quickly grasp
the basic concepts of git. We highly recommend to read through Mark's page

[https://marklodato.github.io/visual-git-guide/index-en.html](https://marklodato.github.io/visual-git-guide/index-en.html)


## Git flight rules

For many operational tasks in aeronautics, it has become standard to have so
called 'flight rules', which give detailed instructions for executing tasks in
a given situation. Similar to aeronautics, you might find yourself in some
problematic situation when you work with git repositories. In this case the git
flight rules might be helpful for you. They provide for most of the common
problems and situations you may encounter with git very specific instructions
how to navigate in this case. So if you have to deal with a certain problem, it
is always worthwhile to check if there is already an answer in the 'git flight
rules':

[https://github.com/k88hudson/git-flight-rules](https://github.com/k88hudson/git-flight-rules)

## Git tutorials on youtube

[https://www.youtube.com/channel/UCicLXXPz3LVEe3lbNDD-zOw](https://www.youtube.com/channel/UCicLXXPz3LVEe3lbNDD-zOw)

## Oh my git!

Oh my git! is an open source game for learning git. Take a look, it's fun

[https://ohmygit.org](https://ohmygit.org)
