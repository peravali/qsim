# Course format

The course will be offered virtually. All the lectures and exercises are taking
place in videoconferences for which extra invitations are provided for all
participants.

Lectures are taking place daily from 9-10am, followed by a short break from
10-10:15am, and then continue from 10:15-11:15am.

After every lecture, hands-on exercises take place from 11:15am-1:00pm, where the
topics of the morning lecture can be practiced in small groups.

For each topic there are exercises at the end which are provided in green
admonition boxes with the following form:

:::{admonition} Exercise
:class: seealso
This is an example for a suggested exercise.
:::

## Remote learning and teaching

As for the course 'Introduction to Python for Computational Science', we use
also for the present course the same setup for remote learning and teaching.
More details for this setup can be found under

  [https://www.desy.de/~fangohr/teaching/py4cs2021/remote.html](https://www.desy.de/~fangohr/teaching/py4cs2021/remote.html)


## Computer accounts

To give all participants a real HPC experience, we provide everybody with a
guest account for the high-performance compute clusters at the Max Planck
Computing and Data Facility in Garching.

This provides also people with a Unix environment which otherwise have not
access to such infrastructure and is also used to introduce slurm basics.

The high-performance compute clusters in Garching all run Linux and access is
only possible using SSH in combination with two-factor authentication.


### Connecting to the MPCDF

Details for login to the MPCDF clusters can be found here

 [https://docs.mpcdf.mpg.de/faq/connecting.html](https://docs.mpcdf.mpg.de/faq/connecting.html)


### Two-factor authentication

The MPCDF employs two-factor authentication (2FA). Besides the usual SSH
password, you therefore also have to provide a one-time-password (OTP), every
time you login.

Details for 2FA can be found here

 [https://docs.mpcdf.mpg.de/faq/2fa.html](https://docs.mpcdf.mpg.de/faq/2fa.html)

:::{admonition} Account activation
:class: seealso
Install the FreeOTP App on your smartphone and follow the instructions on the above
webpage to activate your guest account for the present course.
:::
