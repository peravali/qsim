#!/usr/bin/env python3
# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:hydrogen
#     text_representation:
#       extension: .py
#       format_name: hydrogen
#       format_version: '1.3'
#       jupytext_version: 1.10.3
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# %% [markdown]
# Copyright (c) 2004, 2021, Heiko Appel
# Copyright (c) 2021, Nicolas Tancogne-Dejean
# Copyright (c) 2021, Martin Lueders
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.


# %% [markdown]
# ## FFT Example: Cleaning Electro-Cardiogram data

# %% [markdown]
# Load the required modules:

# %%
import matplotlib.pyplot as plt
import numpy as np
from ipywidgets import interactive, widgets

# %% [markdown]
# Load the data into an array:

# %%
ecg_orig = np.loadtxt("ecg.dat")

# %% [markdown]
# Define the time step and arrays for the time- and frequency-axis:

# %%
delta_t = 2e-3

t = np.arange(0, len(ecg_orig) * delta_t, delta_t)
omega = np.arange(0, 1 / (delta_t), 1 / (len(ecg_orig) * delta_t))


# %% [markdown]
# Define functions to plot time series and spectra:

# %%
def plot_data(x, data, title):
    """
    Plot a time series.
    """
    plt.rcParams["figure.dpi"] = 120

    plt.plot(x, abs(data))
    plt.title(title)
    plt.xlabel("t [s]")
    plt.ylabel("U [a.u.]")
    plt.show()


# %%
def plot_spectrum(x, data, title):
    plt.rcParams["figure.dpi"] = 120

    plt.plot(x, abs(data))
    plt.title(title)
    plt.xlabel("omega [Hz]")
    plt.ylabel("U [a.u.]")
    plt.yscale("log")
    plt.show()


# %% [markdown]
# Let's first plot the original data:

# %%
plot_data(t, ecg_orig, "orig")

# %% [markdown]
# We can use the standard fft routine from `numpy` to generate the frequency
# spectrum.

# %%
spectrum = np.fft.fft(ecg_orig)

# %% [markdown]
# Now we can plot the frequency spectrum:

# %%
plot_spectrum(omega, spectrum, "spectrum")


# %% [markdown]
# It is obvious, that there are two pronounced peaks in the spectrum at 50 Hz
# and 450 Hz. Note that, due to the Nyquist-Shannon sampling theorem,
# frequencies above the Nyquist frequency (here 250 Hz) are ghost images,
# corresponding to $\Omega - \omega$ where $\Omega$ = 500 Hz is the sampling
# frequency.  These 50 Hz peaks are due to the mains frequency of 50 Hz, which
# are insufficiently filtered by the ECG machine. We can also assume that the
# signal should not contain many frequencies higher than that.

# %% [markdown]
# Define a function to filter the spectrum at a given frequency. Remember, that
# due to the above mentioned Nyquist theorem, and in order to conserve the
# symmetry
#
# $$ a(\omega) = a^*(\Omega - \omega) $$
#
# we need to filter symmetrically around $\Omega/2$.

# %%
def filter_spectrum(data, freq, delta_t):

    result = data.copy()
    N = len(data)
    cutoff = int(freq * N * delta_t)

    for i in range(cutoff, int(N / 2) + 1):
        result[i] = 0
        result[N - i] = 0

    return result


# %% [markdown]
# If running this in a life Jupyter notebook, we can utilize the `ipywidgets`
# (see https://ipywidgets.readthedocs.io/en/latest/index.html) to make an
# interacting graph, where we can control the cutoff frequency with a slider,
# and observe the spectra and the filtered data.
#
# First, define a function, which does the filtering and the plotting as
# function the cutoff frequency:

# %% slideshow={"slide_type": "slide"}
def plot_interactive(freq):
    filtered_spec = filter_spectrum(spectrum, freq, delta_t)
    plot_spectrum(
        omega, filtered_spec, "filtered spectrum, cutoff = " + str(freq) + "Hz"
    )
    plot_data(
        t,
        np.fft.ifft(filtered_spec),
        "reconstructed data, cutoff = " + str(freq) + "Hz",
    )


# %% [markdown] slideshow={"slide_type": "fragment"}
# Now we can define the interactive object by passing the above function and a
# widget for the slider, which is linked to the variable `freq`.

# %% slideshow={"slide_type": "fragment"}
interactive_plot = interactive(
    plot_interactive, freq=widgets.IntSlider(min=1, max=250, step=1, value=45)
)

# %% [markdown] slideshow={"slide_type": "fragment"}
# Finally, we define and evaluate the plot object:

# %% slideshow={"slide_type": "slide"}
output = interactive_plot.children[-1]
output.layout.height = "1000px"
interactive_plot

# %% [markdown]
# _Note that the slider is *not* operational when just viewing the jupyter
# book!_
#
# If you adjust the frequency slider, you will notice that the 'noise' will
# appear or disappear, if the cutoff passes 50 Hz. If the cutoff frequency is
# too low, you will observe artificial oscillations, related to the highest
# frequency included in the reconstruction. This could be improved by using a
# smoothed filter function, rather than an abrupt cutoff as in this example.
