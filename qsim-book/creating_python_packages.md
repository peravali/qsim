# Creating Python packages

## Motivation for creating own Python packages

When a Python code base is growing over time, there are several reasons
why the creation of a own Python package becomes advantageous:

 - It may become necessary to run your code not only on a single computer, but
   to run many instances of your program simultaneously on different computers.
   For this it is necessary to run your program 'headless' in a HPC queuing
   system.  Code in Jupyter notebooks is not suited for this.
 - When developing over a longer period, you may gather many purpose tailored
   functions that you created and that you would like to reuse for other
   projects.  It is then useful to group your own code into different own
   packages which you can call for new projects.
 - Longer Jupyter notebooks can become messy and also the order of execution of
   cells can accidentally be not as intended preventing systematic and
   reproducible results.
 - You may want to run your application with different possible input files.
   Reusing code to parse and process input files becomes easier with a package.
 - Having a full source tree in a Python package allows to easily integrate
   unit tests and to perform unit tests before each new commit.

## Python virtual environments

Before we discuss how to create a Python package more in detail, let us digress
for a moment and discuss first Python virtual environments.

Python virtual environments are a useful tool to organize purpose fit software
installations for Python applications. In practice Python applications often
use packages and modules that are not part of the system installation.  Even
more, applications require sometimes a specific version of a library, while
other applications require a different version of the very same library.  It is
clear, that then a single Python installation can not meet the requirements of
every application. Python virtual environments provide a solution to this in
that they allow to have an application specific installation of Python
packages.

### Creating a virtual environment

The simplest way to create a virtual environment is given by the venv module
```shell
> python3 -m venv .venv
```

This creates a Python virtual environment in the folder .venv which then
can be activated with

```shell
source .venv/bin/activate
```

After activation pip and python are available from the path of the
virtual environment and new packages can be installed with pip, e.g.

```shell
pip install -r requirements.txt
```

in case a requirements.txt is available, or manually

```shell
pip install jupyter-book jupyterlab jupytext jupyter_nbextensions_configurator
```

To deactivate the virtual environment you have to execute


```shell
deactivate
```

This removes the search and library paths from your shell environment
so that the packages in the virtual environment are not accessible anymore.


## Creating your own Python packages

### pyscaffold

A good way to start a new Python package that has already a good structure
is to use pyscaffold. pyscaffold allows to create a template package which
can then be used as starting point for all further code developments.

The installation of pyscaffold should be performed in a Python virtual
environment. As described above we then say
```shell
 > python3 -m venv .venv
 > source .venv/bin/activate
 > pip install pyscaffold
```

### Create your own project

Once pyscaffold is installed, we have the command 'putup' at our hands which
allows to create Python package templates.

Create your own Python project template with the following somewhat longish
commandline
```shell
putup --very-verbose --description 'My project' --license gpl-3 --gitlab --pre-commit --venv .venv-myproject myproject
```
Next, we deactivate the virtual environment that we have used for
pyscaffold
```shell
deactivate
```

In the next step, we activate the virtual environment that putup created for us
```shell
 > cd myproject
 > source .venv-myproject/bin/activate
```

Installing a few required packages
```shell
 > pip install jupyter-book jupyterlab jupytext jupyter_nbextensions_configurator matplotlib snakeviz black rise tox
```
It is better to install packages based on a requirements.txt file. This file
contains line by line the packages that we need, possibly with version info.

```shell
 > pip install -r requirements.txt
```

To get a list of all installed packages with their version information,
you can use
```shell
 > pip freeze
```

This can then also be used to generate a requirements.txt
```shell
 > pip freeze > requirements.txt
```

So far, we have installed external packages from [PyPi](https://pypi.org/). Since
we develop here our own package, we can install of course our own package as well to our
virtual environment. Our own package can be install by it's name

```shell
 > pip install myproject
```

This uses the setup.py that the putup command from pyscaffold created for us and
installs the package in the current version

```shell
 > pip freeze | grep myproject
myproject==0.1.0.dev0
```
Alternatively, we can perform a development installation of our own
package
```shell
 > pip install -e .
```

The difference of this developer installation compared to the normal installation
in is that we now can edit our project and changes take immediately effect
without installing the package again.

### Pre commit hooks

If other members of the project clone the new project repository, they should install
locally also the git pre-commit hooks, which can be done with

```shell
 > pre-commit install
```


## Jupyter books for documentation

After you spent a lot of sweat to arrive at your latest scientific findings,
you probably are eager to share your results with collaborators.  Scientific
exchange becomes very effective and efficient by sharing your results over the
web. A particularly useful way of doing this is provided by Jupyter books (even
the present document is a Jupyter book).

You can create a Jupyter book for our qsim project via

```shell
 > jupyter-book create myproject-book
 > jupyter-book build myproject-book
```

### Adding Python scripts as Notebooks

```shell
> jupytext --to notebook qsim-book/Lagrange_interpolation.py
[jupytext] Reading qsim-book/Lagrange_interpolation.py in format py
[jupytext] Writing qsim-book/Lagrange_interpolation.ipynb
```

```shell
> jupytext --update-metadata '{"jupytext":{"formats":"ipynb,py:hydrogen"}}' qsim-book/Lagrange_interpolation.ipynb
[jupytext] Reading qsim-book/Lagrange_interpolation.ipynb in format ipynb
[jupytext] Updating notebook metadata with '{"jupytext": {"formats": "ipynb,py:hydrogen"}}'
[jupytext] Writing qsim-book/Lagrange_interpolation.ipynb (destination file replaced [use --update to preserve cell outputs and ids])
```

```shell
> jupytext --sync qsim-book/Lagrange_interpolation.ipynb
[jupytext] Reading qsim-book/Lagrange_interpolation.ipynb in format ipynb
[jupytext] Loading qsim-book/Lagrange_interpolation.py
[jupytext] Updating the timestamp of qsim-book/Lagrange_interpolation.py
```


### Publishing the book on the web

Both, github.com and gitlab.com provide so called userpages for hosting your
project webpages. This is basically a free hosting service for static webpages.
In addition, both github.com and gitlab.com also provide continuous integration
services.  This allows to execute various tasks after every git push to your
repository.  In combination with userpages and jupyter books this becomes a
quite powerful way to publish and host your content. In order to activate this
functionality for your repository, you need to create a .gitlab-ci.yml file in
the top level directory of the git repository. Fortunately pyscaffold already
created such a file for us. In the following we only adapt this file for our
purposes.

Adding
```shell
image: "python:latest"

variables:
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"
```

to the top of the file is instructing the Gitlab CI service to use the
latest Python Docker image and also to exploit caching of pip packages
for performance reasons.

Furhtermore, we need to add lines with a *pages* section to the
.gitlab-ci.yml file

```shell
pages:
  stage: deploy
  script:
  - apt-get update
  - apt-get -y install rsync
  - pip install -r requirements.txt
  - build_book.sh
  artifacts:
    paths:
    - public
  only:
  - master
```

Here, the build_book.sh script should contain some build process to generate
static html pages which have to be copied to the public folder. The Gitlab CI
will then publish these on the gitlab.io pages site.


## Unit testing

The default configuration that pyscaffold provides also allows to run unit tests.
Unit testing is performed with the package tox. tox aims to automate and standardize
testing in Python. It can be installed with

```shell
 > pip install tox
```

The configuration of tox is contained in the tox.ini file in the top level directory
of the pyscaffold template. The default settings in this config file are already useful
and are sufficient to get started.

Running all unit tests of our own Python package can then be done simply by typing tox

```shell
 > tox
```

In the first run this might take a while since tox is also building up some virtual
environments. All subsequent runs will then be faster.
