## Creating a ssh key

For frequent pull and push operations on gitlab.com it is convenient to
work with SSH keys. To generate such a key run a command similar to the
one below, where you adapt the filename of the key to your case. Make sure
to use a passphrase for your key that is not empty and that is different
from your account password.

```shell
> ssh-keygen -o -t ed25519 -a 100 -f ~/.ssh/id_ed25519.gitlab.com
Generating public/private ed25519 key pair.
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in /home/appel/.ssh/id_ed25519.gitlab.com
Your public key has been saved in /home/appel/.ssh/id_ed25519.gitlab.com.pub
The key fingerprint is:
SHA256:ECYnal6GE7WAAZZKDCrpO/ye6p+U8rrgEE0IsfsZosA appel@miraculix
The key's randomart image is:
+--[ED25519 256]--+
|X=+.+ +          |
|=B = * .         |
|O * + .          |
|=* +   .         |
|=E+     S        |
|++.o.            |
|+=oo             |
|+ * o            |
|.**B             |
+----[SHA256]-----+
```

This key can then be loaded into your SSH agent with
```shell
> ssh-add $HOME/.ssh/id_ed25519.gitlab.com
```

After that you can upload the public part of the key to your profile settings
on gitlab.com. For this go to [Profile](https://gitlab.com/-/profile), and
select on the left SSH Keys. Then output your public key

```shell
> cat $HOME/.ssh/id_ed25519.gitlab.com.pub
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPDVLvfTXO7ldcsnsEhuDUfOyvZSMa4WGQ6NfOsqjLqx appel@miraculix
```

and copy the 'ssh-ed25519 ...' line to the Key field on the webpage.
