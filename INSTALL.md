## Installing the qsim package

After you cloned the repository via
```shell
> git clone git@gitlab.mpcdf.mpg.de:appel/qsim.git
```

you have to create a Python virtual environment
```shell
> cd qsim
> python3 -m venv .venv-qsim
```

and activate the Python virtual environment
```shell
> source .venv-qsim/bin/activate
```

As next step, all project requirements have to be installed
```shell
> pip install -r requirements.txt
```

At this point we have jupyter-book available which can now
be used to generate the Jupyter book from sources. To generate
static HTML pages with the documentation you have to execute

```shell
> qsim-book/build_book.sh
```

The documentation can then be viewed locally on your system
with

```shell
> firefox public/index.html
```

## Code development

First, install the pre-commit hooks
```shell
> pre-commit install
```

Unit tests can be executed with
```shell
> tox
```
