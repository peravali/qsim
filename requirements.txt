-e .
black
jupyter-book
jupyter-c-kernel
jupyter_nbextensions_configurator
jupyterlab
jupytext
matplotlib
pre_commit
rise
snakeviz
tox
