#include <stdio.h>
#include <inttypes.h>

void hexdump(char *data, int length) {
    for (int i=0;i<length;i++) {
        printf(" %02x", *(data+i));
    };
    printf("\n");
};

int main(int argc, char** argv) {
    char* example_data = "moin";
    printf("Different datatype interpretions of the same byte sequence:\n\n");
    printf("         hex:");
    hexdump(example_data, 4);
    printf("\n");
    printf("ASCII  string: %s\n", example_data);
    printf("     int32 le: %" PRId32 "\n", *(int32_t*)example_data);
    int32_t example_data_be = __builtin_bswap32(*(int32_t*)example_data);
    printf("     int32 be: %" PRId32 "\n", example_data_be);
    printf("     float le: %f\n", *(float*)example_data);
    printf("     float be: %f\n", *(float*)&example_data_be);

    return(0);
};
