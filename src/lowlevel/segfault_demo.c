#include <stdio.h>

int main(int argc, char **argv) {
    char * data = "a";
    int accum = 0;
    int pos = 0;
    while (1) {
        accum += (int)data[pos++];
        printf("%8d\n", pos);
    };
    return(0);
};
